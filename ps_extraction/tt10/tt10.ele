!------------------------------------------------------------
! complete TT10 sequence file, K.Hanke M.Giovannozzi 1998
! Modifications by G.Arduini (last 26/03/2002 - checked
! all line)
!
! J. Wenninger, Nov 2005:
!   - convertion to MADX
!   - added a strength parameter as K1 := 'strength' to
!     avoid some bugs in MADX.
!------------------------------------------------------------

! bending magnet polarities as in PS ring:
! positive angles mean bending to the right or upwards

! QUADRUPOLES

! M stands for a string of magnets in the powering

QIID1001.F       : QUADRUPOLE,      L=.8, K1 := KQIID1001;
QIIF1002.F       : QUADRUPOLE,      L=.8, K1 := KQIIF1002;
QIID1003.F       : QUADRUPOLE,      L=.8, K1 := KQIID1003;
QIF1004.F        : QUADRUPOLE,      L=.5, K1 := KQIF1004;
QID1005.F        : QUADRUPOLE,      L=.5, K1 := KQID1005;
QIF1006.F        : QUADRUPOLE,      L=.5, K1 := KQIF1006;
QID1007M.F       : QUADRUPOLE,      L=.5, K1 := KQID1007M;
QIF1008M.F       : QUADRUPOLE,      L=.5, K1 := KQIF1008M;
QID1011M.F       : QUADRUPOLE,      L=.5, K1 := KQID1011M;
QIF1012M.F       : QUADRUPOLE,      L=.5, K1 := KQIF1012M;

QISK1006M.F      : QUADRUPOLE,      L=.5, K1 := KQISK1006M, TILT=-0.785398163397;

! BENDINGS

kmal1001 = 0.0578149;
kmbiv1003 = 0.02976085;
kmbih1000 = 0.0405;

kMDLH1028 = -0.750565E-2;
kMDCV1029 = 0.1972E-3;
kMAL1029  = -0.02906135;
kMDCA1030 = -4.156E-4;
k1bhz = 0;

kbhzvert = 0;
bhz.vert: vkicker, kick := kbhzvert, l = 0.0;


MBIH1000.F       : RBEND,      L=2.5,      ANGLE := kmbih1000, k1 := k1bhz; !(=> BHZ.377/378)
MAL1001M.F       : RBEND,      L=3.4,      ANGLE :=  kmal1001;
MBIV1003M.F      : RBEND,      L=2.5,      ANGLE :=  kmbiv1003, TILT=PI/2;
MBIV1021M.F      : RBEND,      L=2.5,      ANGLE := -MBIV1003M.F->ANGLE, TILT=PI/2;
MDLH1028.F       : RBEND,      L=1.4,      ANGLE := kMDLH1028;
MDCV1029.F       : RBEND,      L=0.3,      ANGLE := kMDCV1029 ,  TILT=PI/2;
MAL1029.F        : RBEND,      L=3.4,      ANGLE := kMAL1029 ;
MDCA1030.F       : RBEND,      L=0.3,      ANGLE := kMDCA1030,   TILT=PI/2;
MSI1183.F        : RBEND,      L=2.0,      ANGLE := KMSI;
MKP.F            : RBEND,      L=3.205,    ANGLE = -0.139823484309E-2;
                                           ! 26 GeV injection - ppbar
                                           ! (in this case negative ang and positive kick)

! STEERERS

MDI              : KICKER,     L=0.25;
MDP              : KICKER,     L=0.25;
MDA              : KICKER,     L=0.424;
MDVI             : VKICKER,    L=0.43;
MDHI             : HKICKER,    L=0.43;

! STEERERS

MDVI1001.F  : MDVI, kick:= KMDVI1001;
MDHI1015.F  : MDHI, kick:= KMDHI1015;
MDVI1016.F  : MDVI, kick:= KMDVI1016;
MDHI1021.F  : MDHI, kick:= KMDHI1021;
MDIH1004A.F : MDI , hkick:= KMDIH1004;
MDIH1004B.F : MDI , hkick:= KMDIH1004;
MDIV1005A.F : MDI , vkick:= KMDIV1005;
MDIV1005B.F : MDI , vkick:= KMDIV1005;
MDIV1021A.F : MDI,  vkick:= KMDIV1021;  // Modified 6/9/2011. Was defined as RBEND
MDIV1021B.F : MDI,  vkick:= KMDIV1021;  // Modified 6/9/2011. Was defined as RBEND
MDIV1025A.F : MDI , vkick:= KMDIV1025;
MDIV1025B.F : MDI , vkick:= KMDIV1025;
MDIH1026A.F : MDI , hkick:= KMDIH1026;
MDIH1026B.F : MDI , hkick:= KMDIH1026;
MDIV1027A.F : MDI , vkick:= KMDIV1027;
MDIV1027B.F : MDI , vkick:= KMDIV1027;

! MONITORS

BPE              : MONITOR    ,    L=0.2;
BTV1             : MONITOR    ,    L=0.46;
BTVTT10          : MONITOR    ,    L=0.45;
BTVOTR           : MONITOR    ,    L=0.45;
BCTTL            : MONITOR    ,    L=0.86;
BFCTTL           : MONITOR    ,    L=0.482;
BSP              : MONITOR    ,    L=0.45;
BPCLTL           : MONITOR    ,    L=0.72;
BSI              : MONITOR    ,    L=0.45;
BSG              : MONITOR    ,    L=0.45;
BSPVSPS          : VMONITOR   ,    L=0.275;

! DUMPS AND COLLIMATORS

TID              : RCOLLIMATOR, L=4.3;
TBSJ             : RCOLLIMATOR, L=1.72;

return;
