# short_desc
AWAKE electron beam line

# desc
Optics models for TT43 electron line, which provides the witness beam for the
proton driven plasma acceleration
