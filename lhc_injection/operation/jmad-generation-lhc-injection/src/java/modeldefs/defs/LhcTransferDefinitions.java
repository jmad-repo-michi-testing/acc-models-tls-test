package modeldefs.defs;

import cern.accsoft.steering.jmad.domain.twiss.TwissInitialConditionsImpl;

final class LhcTransferDefinitions {
    private LhcTransferDefinitions() {
        throw new UnsupportedOperationException("static only");
    }

    static TwissInitialConditionsImpl initialConditions() {
        TwissInitialConditionsImpl twissInitialConditions = new TwissInitialConditionsImpl("lhcinj-twiss");
        twissInitialConditions.setSaveBetaName("LHCINJ.INITBETA0");
        twissInitialConditions.setCalcAtCenter(true);
        twissInitialConditions.setClosedOrbit(false);
        return twissInitialConditions;
    }
}
