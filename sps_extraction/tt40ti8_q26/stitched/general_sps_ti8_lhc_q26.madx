!====================================================
! MADX model of SPS-TT40-TI8 for LHC Q26 optics
!
! F.M.Velotti
!====================================================
 title, "SPS-TT40-TI8 LHC Q20 optics. Protons - 450 GeV/c";

 option, echo;
 option, RBARC=FALSE;

  set, format="22.10e";

/***************************************
* Cleaning .tfs output files
***************************************/

system, "rm *.tfs";
system, "rm ./jmad/*";

/***************************************
* Load needed repos
***************************************/

/* system,"[ -d /afs/cern.ch/eng/acc-models/sps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/sps/2021 sps_repo"; */
system,"[ ! -e sps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-sps -b 2021 sps_repo";

system, "ln -fns ./../../sps_ext_elements sps_extr_repo";
system, "ln -fns ./../../tt40ti8 ti8_repo";

system, "ln -fns ./../line ti8_line_repo";

/***************************************
* TT40-TI8 line model
***************************************/

call,   file = "ti8_repo/ti8.seq";
call,   file = "ti8_repo/ti8_apertures.dbx";
call,   file = "ti8_line_repo/ti8_q26.str";

beam,    sequence=ti8, particle=proton, pc= 450;
use,     sequence=ti8;

ex_g = beam->ex;
ey_g = beam->ey;
dpp = 1.5e-3;

mvar1:= sqrt(table(twiss, betx) * ex_g + (table(twiss, dx) * dpp)^2) * 1e3;
mvar2 := sqrt(table(twiss, bety) * ey_g + (table(twiss, dy) * dpp)^2) * 1e3;

! Errors on b2 and b3 as measured
call, file="ti8_repo/mbi_b3_error.madx";

call, file="./sps_tt40_ti8_lhc_q26.inp";

set_ini_conditions() : macro = {

    INITBETA0: BETA0,
      BETX=BETX0,
      ALFX=ALFX0,
      MUX=MUX0,
      BETY=BETY0,
      ALFY=ALFY0,
      MUY=MUY0,
      T=0,
      DX=DX0,
      DPX=DPX0,
      DY=DY0,
      DPY=DPY0;

};

exec, set_ini_conditions();

select, flag = twiss, clear;
SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, beta0=initbeta0, file="twiss_ti8_q26.tfs";

l_ti8 = table(twiss, LHCINJ.TI8, S);

/*****************************************************************************
Calculate extraction from SPS and prepare for stitching
*****************************************************************************/

call, file = "./load_lhc_q26_extraction.madx";

! Make stitched model of SPS extraction
exec, calculate_stitched_absolute_trajecotry(twiss_sps_ti8_q26_nom.tfs, ti8);

! Save sequence for JMAD
exec, save_stitched_sequence(sps_ti8, q26);

/************************************
* Cleaning up
************************************/

system, "rm -rf sps_repo || rm sps_repo";
system, "rm ti8_line_repo";
system, "rm ti8_repo";
system, "rm sps_extr_repo";

