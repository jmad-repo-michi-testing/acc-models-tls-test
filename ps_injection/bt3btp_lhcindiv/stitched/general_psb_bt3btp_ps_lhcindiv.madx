!==============================================================================================
! MADX file for PSB-BT3-BTP-PS LHC optics
!
! S. Ogur, W. Bartmann, M.A. Fraser, F.M. Velotti
!==============================================================================================
option, echo;

title, "PSB/BT3/BTP/PS optics";


/***************************************
* Load needed repos
***************************************/

system,"[ -d /afs/cern.ch/eng/acc-models/ps/2021 ] && ln -nfs /afs/cern.ch/eng/acc-models/ps/2021 ps_repo";
system,"[ ! -e ps_repo ] && git clone https://gitlab.cern.ch/acc-models/acc-models-ps -b 2021 ps_repo";

stitchps = 1;
call, file ='./../../../psb_extraction/bt3btp_lhc/stitched/general_psb_bt3btp_lhc.madx';

/*****************************************************************************
 Calculate initial conditions for PS injection
*****************************************************************************/

call, file = "./../../ps/load_ps_injection_lhcindiv.madx";

exec, calculate_injection(4.3e-3);

SEQEDIT, SEQUENCE=PS;
       	CYCLE, START=PI.SMH42.ENDMARKER; 
        FLATTEN;
ENDEDIT;

use, sequence=ps;
select, flag=twiss, column=name,keyword,l,s,K0L,K1L,K2L,K3L,x,px,y,py,betx,alfx,bety,alfy,dx,dpx,dy,dpy,mux,muy,angle,K0L,K1L,K2L,K2S,K3L,K3S,VKICK,HKICK;
twiss, sequence=ps, centre=true, beta0 = BETA0INJ;


/*******************************************************************************
  Set nominal initial beam parameters at PS injection (reference frame modified) 
 *******************************************************************************/

call, file = "./../../../psb_extraction/bt3btp_lhc/stitched/ps3_start_lhc.inp";
exec, set_ini_conditions();
INITBETA0->x = x0 + xpsinj;
INITBETA0->px = px0 + pxpsinj;
INITBETA0->dpx = dpx0 + pxpsinj/(beam->beta);

/*******************************************************************************
 * Run twiss for BT3-BTP-PS and stitch result for nominal case
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, sequence=ps, beta0 = INITBETA0;

! Make one single tfs file for both rings and BT-BTP transfer line
len_twiss_ps = table(twiss, tablelength);

i = 1; ! Set index to 1 here because the sequence was cycled and not arbitrary $start marker is present on row = 1
option, -info;
while(i < len_twiss_ps){

    if(i == 1){
        SETVARS, TABLE=nominal, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=nominal;

    i = i + 1;
};
option, info;
write, table=nominal, file="twiss_psb_bt3btp_ps_lhcindiv_nom_complete.tfs";

/*******************************************************************************
  Set kick response initial beam parameters at PS injection (reference frame modified) 
 *******************************************************************************/
call, file = "./../../../psb_extraction/bt3btp_lhc/stitched/ps3_start_kick_response_lhc.inp";
exec, set_ini_conditions();
INITBETA0->x = x0 + xpsinj;
INITBETA0->px = px0 + pxpsinj;
INITBETA0->dpx = dpx0 + pxpsinj/(beam->beta);

/*******************************************************************************
 * Run twiss for BT3-BTP-PS and stitch result for nominal case
 *******************************************************************************/

SELECT, FLAG=TWISS, COLUMN=NAME, KEYWORD,S, L, BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,K1l,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;
twiss, sequence=ps, beta0 = INITBETA0;

! Make one single tfs file for both rings and BT-BTP transfer line
len_twiss_ps = table(twiss, tablelength);

i = 1; ! Set index to 1 here because the sequence was cycled and not arbitrary $start marker is present on row = 1
option, -info;
while(i < len_twiss_ps){

    if(i == 1){
        SETVARS, TABLE=trajectory, ROW = -1;
        s0 = s;
        value, s0;
    }
    SETVARS, TABLE=twiss, ROW=i;
    s = s + s0;
    fill, table=trajectory;

    i = i + 1;
};
option, info;
write, table=trajectory, file="twiss_psb_bt3btp_ps_lhcindiv_kick_response_complete.tfs";

/***********************************************************
* JMAD: prepare single stitched sequences
************************************************************/

/***********************************************************
* PI.SMH42 removed from PS sequence (avoid YASP reading
* septum at end of stitched model)
* PI.SMH42.BTP renamed to PI.SMH42 in BTP sequence (otherwise
* YASP doesn't recognise the septum name)
************************************************************/

SEQEDIT, SEQUENCE=ps;
remove,element=PI.SMH42;
ENDEDIT;

PI.SMH42: rbend,l=0.942,angle:= angleSMH42;

SEQEDIT, SEQUENCE=btp;
REPLACE, ELEMENT=PI.SMH42.btp, BY=PI.SMH42;
ENDEDIT;

EXTRACT, SEQUENCE=psb3, FROM=PSB3.START, TO=BR3.BT_START, NEWNAME=psb3_ext;

psbbt3btpps: SEQUENCE, refer=ENTRY, L  = lpsbext + lbt3 + lbtp + lps;
    psb3_ext        , AT =  0.0000000000 ;
	bt3          	, AT =  lpsbext ;
    btp          	, AT =  lpsbext + lbt3;
    ps              , AT =  lpsbext + lbt3 + lbtp;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = psbbt3btpps;
FLATTEN;
ENDEDIT;

! Matrix to correct for TL reference frame shift
lm1: MATRIX, L=0,  kick1=-xtl, kick2=-pxtl, rm26=-pxtl/(beam->beta), rm51=pxtl/(beam->beta);

! Matrix to correct for TL reference frame shift (BTP to PS)
lm2: MATRIX, L=0,  kick1=xpsinj, kick2=pxpsinj, rm26=pxpsinj/(beam->beta), rm51=-pxpsinj/(beam->beta);

SEQEDIT, SEQUENCE = psbbt3btpps;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR3.BT_START;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

bt3btpps: SEQUENCE, refer=ENTRY, L  =  lbt3 + lbtp + lps;
	bt3         	, AT =  0.0 ;
    btp          	, AT =  lbt3;
    ps              , AT =  lbt3 + lbtp ;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = bt3btpps;
FLATTEN;
ENDEDIT;

SEQEDIT, SEQUENCE = bt3btpps;
FLATTEN;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

btpps: SEQUENCE, refer=ENTRY, L  =  lbtp + lps;
    btp          	, AT =  0;
    ps              , AT =  lbtp ;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = btpps;
FLATTEN;
ENDEDIT;

SEQEDIT, SEQUENCE = btpps;
FLATTEN;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

! Special sequence for HE corrector knob upstream up BT.SMH15

EXTRACT, SEQUENCE=psb3, FROM=BR3.DVT14L1, TO=BR3.BT_START, NEWNAME=smh_ext;

lsmh_ext = lpsbext - 127.93409;

smhbt3btpps: SEQUENCE, refer=ENTRY, L  = lsmh_ext + lbt3 + lbtp + lps;
    smh_ext        , AT =  0.0000000000 ;
	bt3          	, AT =  lsmh_ext ;
    btp          	, AT =  lsmh_ext + lbt3;
    ps              , AT =  lsmh_ext + lbt3 + lbtp;
ENDSEQUENCE;

SEQEDIT, SEQUENCE = smhbt3btpps;
FLATTEN;
ENDEDIT;

SEQEDIT, SEQUENCE = smhbt3btpps;
FLATTEN;
INSTALL, ELEMENT = lm1, at = 0, from=BR3.BT_START;
INSTALL, ELEMENT = lm2, at = 0, from=PI.SMH42.ENDMARKER;
FLATTEN;
ENDEDIT;

! Ensure the kick response is OFF
kBE3KFA14L1 := kBE3KFA14L1REF;

 set, format="22.10e";
use, sequence= psbbt3btpps; 
option, -warn;
save, sequence=psbbt3btpps, beam, file='jmad/psbbt3btpps_lhcindiv.jmad';
option, warn;

 set, format="22.10e";
use, sequence= smhbt3btpps; 
option, -warn;
save, sequence=smhbt3btpps, beam, file='jmad/smhbt3btpps_lhcindiv.jmad';
option, warn;

 set, format="22.10e";
use, sequence= bt3btpps;  
option, -warn;
save, sequence=bt3btpps, beam, file='jmad/bt3btpps_lhcindiv.jmad';
option, warn;

 set, format="22.10e";
use, sequence= btpps;  
option, -warn;
save, sequence=btpps, beam, file='jmad/btpps_lhcindiv.jmad';
option, warn;

/***************************************
* Copy intial conditions to jmad folder
***************************************/

system, "cp psb3_start_lhc.inp jmad";
system, "cp smh3_start_lhc.inp jmad";
system, "cp bt3_start_lhc.inp jmad";
system, "cp btp3_start_lhc.inp jmad";

/*************************************
* Cleaning up
*************************************/

system, "rm twiss_psb_bt3btp_lhc_nom_complete.tfs";
system, "rm twiss_psb_bt3btp_lhc_kick_response_complete.tfs";

system, "rm bt_repo";
system, "rm btp_repo";
system, "rm -rf ps_repo || rm ps_repo";
system, "rm -rf psb_repo || rm psb_repo";

stop;
