!============================================================================ 
!Modified in 2018 by Reyes Alemany to add the new injection BPMs 
!Modified 31.07.2019 by Reyes Alemany to change the EI vertical dipole 
!to SBENDS with angle as defined in Verena's and Alex's optics
!=============================================================================
/**************************************************
 * Septa
 **************************************************/

!MAGNETIC SEPTUM
!DEISMF = 0.175 rad nominal deflection angle, defined in the strength file
LEISM  = 0.825; !Magnetic septum geometric lenght = 0.825 m

!LEISM*DEISMF/SIN(DEISMF) = 0.829226031566 = effective length
EI.SMF      : SBEND, L=LEISM*DEISMF/SIN(DEISMF)
                     , ANGLE=DEISMF, E1=DEISMF, E2=0.0;

!ELECTROSTATIC SEPTUM
!DERSEF = 0.025 rad horizontal deflection angle, defined in the strength file
!IERSE = 6e-3 rad nominal cathod inclination, defined in the strength file
LERSE  = 0.72; !Electrostatic septum cathod lenght = 0.720 m

!LERSE*DERSEF/(SIN(DERSEF-IERSE)+SIN(IERSE)) = 0.720033961006 = effective length
ER.SEF      : SBEND, L=LERSE*DERSEF/(SIN(DERSEF-IERSE)+SIN(IERSE))
                     , ANGLE=DERSEF;

/**************************************************
 * Bending magnets
 **************************************************/
!BHN10
!DEIB1 = 0.330129798755 rad, bending angle, defined in the strength file
!IFR = 0.35 fringe field integral at entrance and exit of the bend, defined in the strength file
LEIB1 = 1.175;
!LEIB1*DEIB1/(2.0*SIN(DEIB1/2.0)) = 1.180352767345
EI.B1       : SBEND, L=LEIB1*DEIB1/(2.0*SIN(DEIB1/2.0)),
                     , ANGLE=DEIB1, E1=DEIB1/2.0, E2=DEIB1/2.0
                     , HGAP=0.07, FINT=IFR;

/*************************************************************************
Reyes Alemany: using the definition in place since 2015 by Alex, Verna
Angle and tilt values defined in strength file
**************************************************************************/
  LEIBV1F = 0.35;
  LEIBV2F = 0.35;

  EI.BV1F      : SBEND, L=LEIBV1F, ANGLE = DEIBV1F, TILT = DEIBV1F_TILT;
  EI.BV2F      : SBEND, L=LEIBV2F, ANGLE = DEIBV2F, TILT = DEIBV2F_TILT;

/**************************************************
 * Quadrupole magnets
 **************************************************/
  EI.Q1       : QUADRUPOLE, L=0.462, K1:=KEI.Q1; ! QFN10
  EI.Q2       : QUADRUPOLE, L=0.462, K1:=KEI.Q2; ! QFN20
  EI.Q3       : QUADRUPOLE, L=0.462, K1:=KEI.Q3; ! QDN30
  EI.Q4       : QUADRUPOLE, L=0.462, K1:=KEI.Q4; ! QFN40

/**************************************************
 * BPMs ordered in the direction of the incoming beam
 **************************************************/
 EI.BPMI10    : MONITOR, L=0.466; !Only BPM, does not take into account the bellow insertions
 EI.BPMI30    : MONITOR, L=0.466; !Only BPM, does not take into account the bellow insertions

/**************************************************
 * Monitors
 **************************************************/
  EI.TV1      : MARKER; ! MTV10
  EI.TV2      : MARKER; ! MTV20


/**************************************************
 * Vacuum chambers
 **************************************************/
  EI.VC010    :    MARKER;
  EI.VC020    :    MARKER;
  EI.VC030    :    MARKER;
  EI.VC040    :    MARKER;
  EI.VC050    :    MARKER;
  EI.VC060    :    MARKER;
  EI.VC070    :    MARKER;
  EI.VC080    :    MARKER;
  EI.VC090    :    MARKER;
  EI.VC100    :    MARKER;
  ER.VCINJ    :    MARKER;